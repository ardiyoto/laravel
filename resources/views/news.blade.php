@extends("template.template-page")

@section("title")
Berita
@stop

@section("content")
    <h2>Ini Page News</h2>
    <p>Ini isinya</p>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Judul</th>
                <th>Isi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($post as $b)
            <tr>                
                <td>{{ $b["id"] }}</td>
                <td>{{ $b["judul"] }}</td>
                <td>{{ $b["isi"] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop