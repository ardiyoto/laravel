@include("inc.header")
<div id="page">
    <div class="wrap">
        <div class="row">
            <div class="content col-md-8">
                @yield("content")
            </div>
            <div class="sidebar col-md-4">
                @yield("sidebar")
            </div>
        </div>
    </div>
</div>
@include("inc.footer")