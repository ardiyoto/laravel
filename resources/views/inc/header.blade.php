<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page @yield("title")</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    @include("inc.sc_head")
</head>
<body>
<!-- Header -->
<header id="header">
    <!-- Top Bar -->
    <div class="wrap">
        <div class="row">
            <div class="logo col-md-4">
                <img src="{{ asset('img/logo.png') }}">
            </div>
            <div class="content col-md-8">
                ini adalah content kanan header
            </div>
        </div>
    </div>
    <!-- Navigation -->
    <div class="menu">
        <div class="wrap">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>