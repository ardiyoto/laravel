<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view("home");
});

Route::get('/about',function(){
    return view("about");
});

Route::get('/berita',function(){
    $blog = [
        ["id"=>"1","judul"=>"Posting Blog 1","isi"=>"This is Content Blog 1"],
        ["id"=>"2","judul"=>"Posting Blog 2","isi"=>"This is Content Blog 2"]
    ];

    return view("news",["post"=>$blog]);
    //return view("news")->with("post",$blog);
    //return view("news",compact("blog"));
});

Route::get('/berita/{id}',function($id){
    echo $id;
});






